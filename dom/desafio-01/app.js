new Vue({
    el: '#desafio',
    data:{
        nome: 'Eliezer',
        idade: 23,
        imageUrl: 'https://www.musitechinstrumentos.com.br/files/pro_20706_e.jpg'
    },
    methods: {
        multiplicaIdade(valor){
            return this.idade * valor
        },
        random(){
            return Math.random()
        }
    },

})